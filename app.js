const htmlStandards = require("reshape-standard");
const cssStandards = require("spike-css-standards");
const jsStandards = require("spike-js-standards");
const preactBabel = require("babel-preset-preact");
const pageId = require("spike-page-id");
const fs = require("fs");
const lookup = require('postcss-property-lookup');

const icon = name => fs.readFileSync(`assets/icons/${name}.svg`);

module.exports = {
  devtool: "source-map",
  matchers: {
    html: "*(**/)*.sgr",
    css: "*(**/)*.sss"
  },
  ignore: [
    "**/layout.sgr",
    "assets/**/_*",
    "**/.*",
    "readme.md",
    "yarn.lock",
    "**/*.svg"
  ],
  reshape: htmlStandards({
    locals: ctx => ({ pageId: pageId(ctx) }),
    content: { icon },
    minify: true
  }),
  postcss: cssStandards({
    prependPlugins: [lookup],
    minify: true,
    warnForDuplicates: false
  }),
  babel: jsStandards({ appendPresets: [preactBabel] })
};
